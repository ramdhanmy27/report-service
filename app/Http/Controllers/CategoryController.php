<?php

namespace App\Http\Controllers;

use App\Support\Category\CategoryAdapter;
use Illuminate\Http\Request;

class CategoryController extends Controller {

    public function find(Request $req, $source) {
        $adapter = new CategoryAdapter();
        $source = $source == "sdmx" ? "eurostat" : $source;
        $res = $adapter->findByKey($source, $req->input("key"));

        return $res !== null ? $res : [];
    }
}
