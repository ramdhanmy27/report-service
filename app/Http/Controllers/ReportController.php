<?php

namespace App\Http\Controllers;

use Cache;
use Carbon\Carbon;
use FluidXml\FluidXml;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Hobnob\XmlStreamReader\Parser;
use Illuminate\Http\Request;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\StringWalker;
use Prewk\XmlStringStreamer\Stream;
use Sabre\Xml\Service;

class ReportController extends Controller {

	private $guzzle;
	private $cookieJar;

	public function __construct() {
		$this->cookieJar = new CookieJar();
		$this->guzzle = new Client([
			"base_uri" => config("jasper.endpoint"),
			"cookies" => $this->cookieJar,
		]);

		Cache::flush();

		if (!Cache::has("jasper_cookies")) {
	    	$this->login();
	    	$this->cookieJar = Cache::get("jasper_cookies");
    	}
	}

	public function login($j_username = "jasperadmin", $j_password = "jasperadmin") {
		$this->guzzle->request("POST", "login", ["form_params" => compact("j_username", "j_password")]);
		Cache::put("jasper_cookies", $this->cookieJar, Carbon::now()->addMinutes(10));
	}

    public function getReportList(Request $req) {
		$res = $this->guzzle->request("GET", "resources", [
			"query" => [
				"recursive" => 1,
				"type" => "reportUnit",
				"q" => $req->input("search", ""),
				// "limit" => $req->input("limit", 10),
				// "offset" => $req->input("offset", 0),
			],
		]);

		$data = [];

		foreach ((new Service())->parse($res->getBody()->getContents()) as $node) {
			$data[] = [
	    		"path" => $node["attributes"]["uriString"],
	    		"name" => $node["value"][0]["value"],
	    		"description" => $node["value"][1]["value"],
	    		"created" => $node["value"][2]["value"],
	    	];
		}

		return $data;
    }

    public function getResourceDescriptor($path) {
    	$res = $this->guzzle->request("GET", "resource/$path");
		$desc = simplexml_load_string((string) $res->getBody());

		return [
			"path" => (string) $desc["uriString"],
			"label" => (string) $desc->label,
			"description" => (string) $desc->description,
			"created" => (string) $desc->creationDate,
		];
    }

    public function export(Request $req, $type = "PDF") {
    	$report_path = $req->input("file");

    	// get resource descriptor
    	$rd_res = $this->guzzle->request("GET", "resource$report_path");

    	// get report UUID
    	$uuid_res = $this->guzzle->request("PUT", "report", [
    		"query" => ["RUN_OUTPUT_FORMAT" => $type],
    		"body" => $rd_res->getBody(),
    		"headers" => ["Content-Type" => "text/xml; charset=UTF8"],
    	]);

    	// get the report file
		$report_xml = simplexml_load_string((string) $uuid_res->getBody());
		$uuid = (string) $report_xml->uuid;

		$lc_type = strtolower($type);
		$filepath = storage_path("app/public/$uuid.$lc_type");
    	$file_res = $this->guzzle->request("GET", "report/$uuid", [
    		"query" => ["file" => "report"],
    		"sink" => $filepath,
    	]);

    	switch ($lc_type) {
    		case 'html': return response()->file($filepath);
    		default: return response()->download($filepath, "report.$lc_type")->deleteFileAfterSend(true);
    	}
    }
}
