<?php

namespace App\Support\Category;

use App\Support\Category\EurostatAdapter;
use App\Support\Category\FameAdapter;

class CategoryAdapter {

    public function __construct() {}

    public function findByKey($source, $key = null) {
        switch ($source) {
            case "eurostat":
                $adapter = new EurostatAdapter();
                break;

            case "fame":
                $adapter = new FameAdapter();
                break;

            default: return;
        }

        return $adapter->findByKey($key);
    }
}