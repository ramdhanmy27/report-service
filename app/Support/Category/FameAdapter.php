<?php

namespace App\Support\Category;

use GuzzleHttp\Client;

class FameAdapter {

    private $guzzle;

    public function __construct() {
        $this->guzzle = new Client([
            "base_uri" => config("fame.endpoint"),
        ]);
    }

    public function findByKey($key = null, $depth = 1) {
        $categories = $key === null ? $this->getRoot() : $this->getChildren($key);

        return array_map(function($item) {
            return [
                "code" => $item->code === "?" ? $item->EnglishName : $item->code,
                "name" => $item->EnglishName,
                "type" => "category",
            ];
        }, $categories);
    }

    private function getRoot() {
        $res = $this->guzzle->request("POST", "GetFameObjectsService/GetNodes", [
            "form_params" => [
                "ENV" => "Meta",
                "LC" => "EN",
                "DB" => "sama_scrn",
                "FS" => "?",
            ],
        ]);

        return json_decode($res->getBody()->getContents());
    }

    private function getChildren($key) {
        $res = $this->guzzle->request("POST", "GetFameObjectsService/GetChildNodes", [
            "form_params" => [
                "ENV" => "Meta",
                "LC" => "EN",
                "DB" => "fedm_scrn",
                "FS" => "$key.",
            ],
        ]);

        return json_decode($res->getBody()->getContents());
    }
}