<?php

return [
    'endpoint' => env('JASPER_ENDPOINT', 'http://localhost:8080/jasperserver/rest/'),
    'endpoint.v2' => env('JASPER_ENDPOINT_V2', 'http://localhost:8080/jasperserver/rest_v2/'),
];
