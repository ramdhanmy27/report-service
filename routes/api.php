<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("v1")->middleware("api")->group(function () {
    Route::get("reports", "ReportController@getReportList");
    Route::get("report/desc/{path}", "ReportController@getResourceDescriptor")->where("path", "(.*)");
    Route::get("report/export/{type}", "ReportController@export");

    Route::get("category/{source}", "CategoryController@find");
});
